@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Crear un cliente</div>
                <div class="col text-right">
                    <a href="{{route('list.cliente')}}" class="btn-sm btn-succes">Cancelar</a>
                </div>
                <div class="card-body">
                    <form role="form" method="post" action="{{route('guardar.cliente')}}">
                        {{csrf_field()}}
                        {{method_field('post')}}
                    <div class="row">
                    <div class="col-lg-4">
                        <label class="from-control-label" fro="Nombre">Nombre del cliente</label>
                        <input type="text" class="from-control" name="Nombre">
                </div>

                <div class="col-lg-4">
                        <label class="from-control-label" fro="Apellido">Apellido del cliente</label>
                        <input type="text" class="from-control" name="Apellido">
                </div>

                <div class="col-lg-4">
                        <label class="from-control-label" fro="Cedula">Cedula del cliente</label>
                        <input type="number" class="from-control" name="Cedula">
                </div>

                <div class="col-lg-4">
                        <label class="from-control-label" fro="Direccion">Direccion del cliente</label>
                        <input type="text" class="from-control" name="Direccion">
                </div>
                <div class="col-lg-4">
                        <label class="from-control-label" fro="Telefono">Telefono del cliente</label>
                        <input type="number" class="from-control" name="Telefono">
                </div>
                <div class="col-lg-4">
                        <label class="from-control-label" fro="Fecha_de_Nacimiento">Fecha de Nacimiento del cliente</label>
                        <input type="text" class="from-control" name="Fecha_de_Nacimiento">
                </div>
                <div class="col-lg-4">
                        <label class="from-control-label" fro="Email">Email del cliente</label>
                        <input type="text" class="from-control" name="Email">
                </div>

                </div>

                <button type="submit" class="btn btn-success pull-right">Guardar</button>
                </form>

              </div>
            </div>
        </div>
    </div>
</div>
@endsection