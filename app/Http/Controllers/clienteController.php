<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cliente;

class clienteController extends Controller
{

    public function Iniciocliente(Request $request)
    {
        $cliente = cliente::all();
       return view('cliente.inicio')->with('cliente', $cliente);
    }

    public function Crearcliente(Request $request)
    {
        $cliente = cliente::all();
        return view('cliente.crear')->with('cliente', $cliente);
    }
    
    public function Guardarcliente(Request $request){
        $this->validate($request, [
            'Nombre'         => 'required',
            'Apellido'       => 'required',
            'Cedula'         => 'required',
            'Direccion'      => 'required',
            'Telefono'        => 'required',
            'Fecha_de_Nacimiento'=> 'required',
            'Email'              => 'required'
        ]);

        $cliente = new cliente;
        $cliente->Nombre=$request->Nombre;
        $cliente->Apellido=$request->Apellido;
        $cliente->Cedula=$request->Cedula;
        $cliente->Direccion=$request->Direccion;
        $cliente->Telefono=$request->Telefono;
        $cliente->Fecha_de_Nacimiento=$request->Fecha_de_Nacimiento;
        $cliente->Email=$request->Email;
        $cliente->save();
        return redirect()->route('list.cliente');
    }
}