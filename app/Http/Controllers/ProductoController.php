<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\producto;

class ProductoController extends Controller
{

    public function InicioProducto(Request $request)
    {
        $producto = producto::all();
       return view('productos.inicio')->with('producto', $producto);
    }

    public function CrearProducto(Request $request)
    {
        $producto = Producto::all();
        return view('productos.crear')->with('producto', $producto);
    }
    
    public function GuardarProducto(Request $request){
        $this->validate($request, [
            'Nombre' => 'required',
            'Tipo'=> 'required',
            'Estado'=> 'required',
            'Precio'=> 'required'
        ]);

        $producto = new Producto;
        $producto->Nombre=$request->Nombre;
        $producto->Tipo=$request->Tipo;
        $producto->Estado=$request->Estado;
        $producto->Precio=$request->Precio;
        $producto->save();
        return redirect()->route('list.productos');
    }
}