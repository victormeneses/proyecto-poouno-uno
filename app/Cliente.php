<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $fillable = [
        'Nombre',
        'Apellido',
        'Cedula',
        'Direccion',
        'Telefono',
        'Fecha_de_Nacimiento',
        'Email'
    ];
}
